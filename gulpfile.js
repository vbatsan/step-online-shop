const gulp = require("gulp");
const sass = require("gulp-sass"),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    ugli = require('gulp-uglifyjs'),
    cleanCSS = require('gulp-clean-css'),
    autoprefix = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    cache = require('gulp-cache'),
    babel = require('gulp-babel'),
    clear = require('gulp-clean');

gulp.task('html', function(){
    return gulp.src('./index.html')
    .pipe(browserSync.stream())
})

gulp.task ('sass', function () {
    return gulp.src("src/sass/main.scss")
        .pipe(sass().on('error', notify.onError(function () {
            return '<%= error.message %>'
        })))
        .pipe(autoprefix())
        .pipe(concat('style.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream())

});

gulp.task('clean', function () {
    return gulp.src('./dist/**/*', {
        read: false
    })
        .pipe(clear())
});

gulp.task('script', function () {
    return gulp.src(['./src/js/rater.js', './src/js/furnitureLoadMore.js', './src/js/fur-carousel.js', './src/js/slider-menu.js'])
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('script.min.js'))
        .pipe(ugli())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream())

});

gulp.task('img', function () {
    return gulp.src('src/img/**')
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))

});

gulp.task ('watch', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch("src/sass/**/*.scss", gulp.series('sass'));
    gulp.watch("src/js/**/*.js", gulp.series('script'));
    gulp.watch("src/img/**/*", gulp.series('img'));
    gulp.watch("./index.html", gulp.series('html'));
});

gulp.task('build', gulp.series('clean', 'sass', 'script', 'img'));
gulp.task('dev', gulp.series('sass', 'script', 'watch'));


